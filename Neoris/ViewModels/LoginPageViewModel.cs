﻿using Neoris.Models;
using Neoris.Views;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;

namespace Neoris.ViewModels
{
    public class LoginPageViewModel : ViewModelBase
    {

        public ICommand LoginCommand { get; set; }
        public UserLogin _User { get; set; }
        public LoginPageViewModel(INavigationService navigationService)
            : base(navigationService)
        {
            Init();
        }
        private void Init()
        {
            LoginCommand = new Command(LoginFunction);
            _User = new UserLogin();
        }

        private void LoginFunction()
        {
            if (_User != null)
                if (_User.User == "Ramiro" && _User.Password == "1234")
                {
                    NavigationService.NavigateAsync(nameof(PageTwo));
                }
        }
    }
}
