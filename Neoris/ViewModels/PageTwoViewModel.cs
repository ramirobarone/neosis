﻿using Prism.Navigation;
using System;
using System.Collections.Generic;
using System.Text;

namespace Neoris.ViewModels
{
    public class PageTwoViewModel : ViewModelBase
    {
        public PageTwoViewModel(INavigationService navigation) : base(navigation)
        {

        }
    }
}
