﻿using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace Neoris.Models
{
    public class UserLogin : BindableBase
    {
        private string _User;
        private string _Password;

        public string User
        {
            get => _User;
            set => SetProperty(ref _User, value);
        }
        public string Password
        {
            get => _Password;
            set => SetProperty(ref _Password, value);
        }

    }
}
