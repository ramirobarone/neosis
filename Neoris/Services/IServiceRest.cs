﻿using Neoris.Models;

namespace Neoris.Services
{
    public interface IServiceRestLogin : IServiceGet
    {
    }
    public interface IServiceGet
    {
        bool Login(UserLogin user);
    }
}
